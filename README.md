tclx conda recipe
=================

Home: http://tclx.sourceforge.net/

Package license: BSD

Recipe license: BSD 2-Clause

Summary: Extends Tcl by providing new operating system interface commands, extended file control, scanning and status commands and many others.
